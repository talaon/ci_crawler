<?php
class Redisclient{
	private $_redis = null;
	public function __construct(){
		$this->_redis = new Redis();

		$CI = &get_instance();
		$CI->config->load('redis_config');
		$sHost = $CI->config->item("redis_host");
		$sPort = $CI->config->item("redis_port");
		$this->_redis->connect($sHost, $sPort);
	}

	public function addToSet($sKey, $sEleName){
		if(empty($sKey)){
			return false;
		}

		$this->_redis->sAdd($sKey, $sEleName);
	}

	public function isInSet($sKey, $sEleName){
		return $this->_redis->sIsMember($sKey, $sEleName);
	}

	public function inQueue($sQueueName, $sQueueValue){
		return $this->_redis->lPush($sQueueName, $sQueueValue);
	}

	public function deQueue($sQueueName){
		return $this->_redis->brPop($sQueueName, 10);
	}

	public function getQueueLen($sQueueName){
		return $this->_redis->lSize($sQueueName);
	}
}