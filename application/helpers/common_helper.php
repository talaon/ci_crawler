<?php
	function toUrlParams($values){
		$buff = "";
		foreach ($values as $k => $v)
		{
			if($k != "sign" && $v != "" && !is_array($v)){
				$buff .= $k . "=" . $v . "&";
			}
		}
		
		$buff = trim($buff, "&");
		return $buff;
	}

	//生成签名
	function makeSign($values){
		//签名步骤一：按字典序排序参数
		ksort($values);
		$string = toUrlParams($values);
		//签名步骤二：在string后加入KEY
		//在这无法使用$this->config->load
		$CI =& get_instance();
		$CI->config->load('wxpay');
		$sKey = $CI->config->item('key');
		$string = $string . "&key=". $sKey;
		//签名步骤三：MD5加密
		$string = md5($string);
		//签名步骤四：所有字符转为大写
		$result = strtoupper($string);
		return $result;
	}


	function log_warning($sContent){
		$log_dir = __DIR__ . '/../log_' . date('Y-m-d') . '.log';
		file_put_contents($log_dir, date('Y-m-d H:i:s') . ':' . $sContent . PHP_EOL, FILE_APPEND);
	}