<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$config['redis_host'] = '127.0.0.1';
$config['redis_port'] = 6379;