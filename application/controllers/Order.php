<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Order extends CI_Controller{
	public function order_pay(){
		//1.读取用户输入 get post cookie session
		//2. 取数据
		//3. 返回数据 一 拼装html 二 echo json_encode(); protobuf:pb数据
		//$movie_id = $_GET['id'];
		$movie_id = $this->input->get("id");
		$this->load->model('Movie');
		$aRes = $this->Movie->getMovieInfo($movie_id);
		//var_dump($aRes);
		//echo json_encode($aRes);
		//View:视图层
		$this->load->view('order_pay', $aRes);
	}

	public function test_curl(){
		$this->load->helper("curl");
		$res = curl("http://www.baidu.com");
		echo htmlentities($res);//htmlentities让html字符转义 变为普通可以展示字符
	}

	public function test_frame(){
		echo "hello ci framework!";
	}
}