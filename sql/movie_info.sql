CREATE TABLE `movie_info`(
	`id` int(11) primary key auto_increment COMMENT '主键',
	`movie_id` varchar(20) default '' COMMENT '豆瓣电影的主键',
	`movie_name` varchar(200) default '' COMMENT '电影名字',
	`movie_rate` decimal(10,1) default '0.0' COMMENT '电影评分',
	`movie_director` varchar(20) default '' COMMENT '电影导演',
	`movie_main` varchar(200) default '' COMMENT '电影主演',
	`movie_type` varchar(200) default '' COMMENT '电影类型',
	`movie_time` date default '0000-00-00' COMMENT '电影上映时间',
	`movie_length` varchar(200) default '' COMMENT '电影长度',  
 	`create_time` timestamp default '0000-00-00 00:00:00' COMMENT '创建时间',
	`modify_time` timestamp default '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
	`status` tinyint(3) default '1' COMMENT '是否有效',
	KEY `idx_movie_id` (`movie_id`)
)Engine=Innodb;